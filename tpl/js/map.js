/* MAPBOX CONFIGURATION*/
var map = L.mapbox.map('map', 'jacopodalpozzo.i7pbeoc7', {
	// Disable default double-click behavior.
	doubleClickZoom: true
});

/* OMNIVORE */
// Omnivore will AJAX-request this file behind the scenes and parse it:
// The file must either be on the same domain as the page that requests it, or both the server it is requested from and the user's browser must support CORS.

/* Load Poligon */
var runLayer = omnivore.kml('./data/trentino.kml')
	.on('ready', function() {
		map.fitBounds(runLayer.getBounds());

		// After the 'ready' event fires, the GeoJSON contents are accessible and you can iterate through layers to bind custom popups.
		runLayer.eachLayer(function(layer) {
			// See the `.bindPopup` documentation for full details. This dataset has a property called `name`: your dataset might not, so inspect it and customize to taste.
			layer.bindPopup(layer.feature.properties.name);
		});
	})
	.addTo(map);

/* LEGEND */
// Find and store a variable reference to the list of filters.
var filters = document.getElementById('filters');

// Wait until the marker layer is loaded in order to build a list of possible types. If you are doing this with another featureLayer, you should change map.featureLayer to the variable you have assigned to your featureLayer.
map.featureLayer.on('ready', function() {
	// Collect the types of symbols in this layer. You can also just hardcode an array of types if you know what you want to filter on, like var types = ['foo', 'bar'];
	var typesObj = {}, types = [];
	var features = map.featureLayer.getGeoJSON().features;
	for (var i = 0; i < features.length; i++) {
		typesObj[features[i].properties['marker-symbol']] = true;
	}
	for (var k in typesObj) types.push(k);

	var checkboxes = [];
	// Create a filter interface.
	for (var i = 0; i < types.length; i++) {
		// Create an <li> list element for each type, and add an input checkbox and label inside.
		var li = filters.appendChild(document.createElement('li'));
		var checkbox = li.appendChild(document.createElement('input'));
		var label = li.appendChild(document.createElement('label'));
		checkbox.type = 'checkbox';
		checkbox.id = types[i];
		checkbox.checked = true;
		// Create a label to the right of the checkbox with explanatory text.
		label.innerHTML = types[i];
		label.setAttribute('for', types[i]);
		// Whenever a person clicks on this checkbox, call the update() function below.
		checkbox.addEventListener('change', update);
		checkboxes.push(checkbox);
	}

	// This function is called whenever someone clicks on a checkbox and changes the selection of markers to be displayed.
	function update() {
	var enabled = {};
	// Run through each checkbox and record whether it is checked. If it is, add it to the object of types to display, otherwise do not.
	for (var i = 0; i < checkboxes.length; i++) {
		if (checkboxes[i].checked) enabled[checkboxes[i].id] = true;
	}
	map.featureLayer.setFilter(function(feature) {
		// If this symbol is in the list, return true. if not, return false. The 'in' operator in javascript does exactly that: given a string or number, it says if that is in a object.
		// 2 in { 2: true } // true
		// 2 in { } // false
		return (feature.properties['marker-symbol'] in enabled);
	});
	}
});

/* TOOLTIPS ON HOOVER */
var myLayer = L.mapbox.featureLayer().addTo(map);
myLayer.setGeoJSON(geoJson);
myLayer.on('mouseover', function(e) {
	e.layer.openPopup();
});
myLayer.on('mouseout', function(e) {
	e.layer.closePopup();
});

/* LOCATION */
L.control.locate().addTo(map);

/* FULLSCREEN */
L.control.fullscreen().addTo(map);

/* CENTERING MARKERS */
map.featureLayer.on('click', function(e) {
	map.panTo(e.layer.getLatLng());
});