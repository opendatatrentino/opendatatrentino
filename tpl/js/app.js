/**
 * Created by lorenzo on 01/05/14.
 */

$(".statistic").hide();
$('input').keypress(function (event) {
    if (event.which == 13) {
        /* FOCUS ELEMENT */

        $(".statistic").hide("fast");
        $("#multi_chois").html("");
        $.ajax({
            type: 'GET',
            url: "/ws/comune?com=" + $(this).val(),
            data: {},
            dataType: 'json',
            success: function (data) {
                console.log(data);
                resultFromSearchField(data);

            }, error: function (jqXHR, textStatus, errorThrown) {
                console.log(JSON.stringify(errorThrown));
                console.log(textStatus);
            }
        });

        return false;
    }
});


function resultFromSearchField(result) {
    $('.city').remove();
    $('.result_stat').remove();
    //var obj = JSON.parse(result);
    $("#multi_chois").html("");
    if (Object.prototype.toString.call(result) === '[object Array]') {

        if (result.length > 1) {

            for (var i = 0; i < result.length; i++) {
                console.log("<a href='#' class='choise'>" + result[i].Solo_denominazione_in_italiano + "</a> ");
                $("#multi_chois").append("<a href='#' class='choise'>" + result[i].Solo_denominazione_in_italiano + "</a> ");
            }
            $(".choise").click(function () {
                $.ajax({
                    type: 'GET',
                    url: "/ws/comune?com=" + $(this).html(),
                    data: {},
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        resultFromSearchField(data);

                    }, error: function (jqXHR, textStatus, errorThrown) {
                        console.log(JSON.stringify(errorThrown));
                        console.log(textStatus);
                    }
                });

            });
        }

    } else if (Object.prototype.toString.call(result) === '[object Object]') {

        $('.index-title').append("<p class='city'>di "+result.Solo_denominazione_in_italiano+"</p>");
        $(".statistic").show("fast");
        if (result.latitudine && result.longitudine)
            map.panTo([result.latitudine , result.longitudine]).setZoom(14);
        $("ul#index.statistic li   a").each(function (index, element) {
            switch (index) {
                //case 0:
                //    $(element).append("<div class='result_stat' >ND</div>");
                 //   break;
                case 0:
                    $(element).append("<div class='result_stat' >"+ result.Posti_letto_Ordinari+ "</div>");
                    break;
                case 1:
                    $(element).append("<div class='result_stat' >" + result.Posti_letto_Day_Hospital+ "</div>");
                    break;
                case 2:
                    $(element).append("<div class='result_stat' >"+ result.Posti_letto_Day_Surgery+ "</div>");
                    break;
                case 3:
                    $(element).append("<div class='result_stat' >"+ result.posti_letto+"</div>");
                    break;
                default:
            }

        });
    }

}
