// kick things off
window.onload = createPieChart;

function createPieChart() {

//
// create a PieChart.
//
var pieChart = new PieChart( "piechart",
{
includeLabels: true,
data: [31.6, 30.5, 31.9, 31.4, 29.7, 28.7, 27.3, 26.4],
labels: ["2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010"],
colors: [
             ["#FFDAB9", "#FFDAB9"],
             ["#E6E6FA", "#E6E6FA"],
             ["#E0FFFF", "#E0FFFF"],
             ["#F0DAB9", "#F0DAB9"],
             ["#D6E6FA", "#D6E6FA"],
             ["#E000FF", "#E000FF"],
	     ["#50FFFF", "#50FFFF"]		
]
}
);

//
// nothing appears until you call draw().
//
pieChart.draw();

/*
* If you want to draw the labels separately, you can set
* includeLabels to false, and call drawLabel() for each
* pie chart segment.
*
for (var i = 0; i < pieChart.labels.length; i++) {
pieChart.drawLabel(i);
}
*/

/*
* If you want to select a segment to highight it, you can
* call select() for a given segment. Here's a little snippet
* that animates selecting each segment.
*
var segment = 0;
function nextSegment() {
pieChart.select(segment);
segment++;
if (segment < pieChart.data.length) {
setTimeout(nextSegment, 1000);
}
}
setTimeout(nextSegment, 1000);
*/

}
