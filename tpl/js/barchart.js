 window.onload = function () {
        var chart = new CanvasJS.Chart("chartContainer", {
            
            axisY: {
                tickThickness: 0,
                lineThickness: 0,
                valueFormatString: " ",
                gridThickness: 0                    
            },
            axisX: {
                tickThickness: 0,
                lineThickness: 0,
                labelFontSize: 16,
                labelFontColor: "Peru"

            },
            data: [
            {
                indexLabelFontSize: 26,
                toolTipContent: "<span style='\"'color: {color};'\"'><strong>{indexLabel}</strong></span> <span style='\"'font-size: 12px; color:peru '\"'><strong>{y}%</strong></span>",

                indexLabelPlacement: "inside",
                indexLabelFontColor: "white",
                indexLabelFontWeight: 400,
                indexLabelFontFamily: "Arial",
                color: "#62C9C3",
                type: "bar",
                dataPoints: [
                    { y: 26.4, label: "26.4%", indexLabel: "2010" },
                    { y: 27.3, label: "27.3%", indexLabel: "2009" },
                    { y: 28.7, label: "28.7%", indexLabel: "2008" },
                    { y: 29.7, label: "29.7%", indexLabel: "2007" },
                    { y: 31.4, label: "31.4%", indexLabel: "2006" },
                    { y: 31.9, label: "31.9%", indexLabel: "2005" },
                    { y: 30.5, label: "30.5%", indexLabel: "2004" },
                    { y: 31.6, label: "31.6%", indexLabel: "2003" }

                ]
            }
            ]
        });

        chart.render();
    }
