﻿<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="author" content="Jacopo Dal Pozzo, Steven Tait, Lorenzo Waldner /">
    <meta name="description" content="Project developed for the Trentino Open Data Challenge 2014"/>
    <meta name="keywords" content="Open Data, OpenData, Dati Aperti, Trentino, Open Data Challenge, Trento Rise, Waldner, Tait, Dal Pozzo"/>
    <meta name="robots" content="index, follow"/>

    <title>Open M.A.T.I.S.</title>

    <link rel="icon" type="image/png" href="/icons/favicon.png"/>
    <link rel="apple-touch-icon" type="image/png" href="/icons/favicon.png"/>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="css/style.css" rel="stylesheet"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/jquery-cookie.js" charset="utf-8" type="text/javascript"></script>
    <script src="js/jquery-lang.js" charset="utf-8" type="text/javascript"></script>
    <script type="text/javascript"> 
	window.lang = new Lang('en');
        window.lang.dynamic('it', 'js/langpack/it.json');
        window.lang.dynamic('de', 'js/langpack/de.json');
    </script>

    <!-- Mapbox -->
    <script src='https://api.tiles.mapbox.com/mapbox.js/v1.6.2/mapbox.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox.js/v1.6.2/mapbox.css' rel='stylesheet'/>

    <!-- Leaflet LOCALISATION-->
    <script src='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.24.0/L.Control.Locate.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.24.0/L.Control.Locate.css' rel='stylesheet'/>
    <!--[if lt IE 9]>
    <link href='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.21.0/L.Control.Locate.ie.css' rel='stylesheet'/>
    <![endif]-->

    <!-- Leaflet FULLSCREEN -->
    <script src='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v0.0.2/Leaflet.fullscreen.min.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v0.0.2/leaflet.fullscreen.css' rel='stylesheet'/>

    <!-- Leaflet OMNIVORE -->
    <script src='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-omnivore/v0.1.1/leaflet-omnivore.min.js'></script>

    <!-- PieChart -->
    <script src="./js/piechart.js" charset="utf-8" type="text/javascript"></script>
    <script src="./js/pie.js" charset="utf-8" type="text/javascript"></script>

    <!-- BarChart -->
    <script src="./js/barchart.js" charset="utf-8" type="text/javascript"></script>
    <script src="./js/canvasjs.min.js" charset="utf-8" type="text/javascript"></script>

</head>
<body>

<div id="navbar" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only" lang="en">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Open M.A.T.I.S.</a>
        </div>
        <div class="navbar-collapse collapse">
            <form class="navbar-form navbar-right">
                <input type="text" class="form-control" lang="en" placeholder="Search">
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#mission" lang="en"><img src="./icons/bulb.png" alt="Mission"/> Mission</a></li>
                <li><a href="#guide" lang="en"><img src="./icons/guide.png" alt="Guide"/> Guide</a></li>
                <li><a href="#download" lang="en"><img src="./icons/download.png" alt="Dowload"/> Download</a></li>
                <li><a href="#info" lang="en"><img src="./icons/info.png" alt="Info"/> Information</a></li>
                <li><a href="#"></a></li>
                <li><a href="#" onclick="window.lang.change('en'); return false;"><img src="/icons/en.png" alt="English"/></a></li>
                <li><a href="#" onclick="window.lang.change('it'); return false;"><img src="/icons/it.png" alt="Italiano"/></a></li>
                <!--<li><a href="#" onclick="window.lang.change('de'); return false;"><img src="/icons/de.png" alt="Deutsch" /></a></li>-->
            </ul>

            <div id="mission" class="modalDialog">
                <div>
                    <a href="#close" class="close"><img src="./icons/close.png" alt="X"/></a>
                    <h2 lang="en">Mission</h2>
                    <p lang="en">Open Data are a synonym of transparency. They are a tool for citizens, in the service of the democratic freedom, a service for Europe. Basing our work on these values and motivated by the intention to make information available to all citizens, we have created an easy tool to understand and to inform people about health services available in Trentino.<p>
                    <p lang="en">Soon it will be also extended into a service based on the regions and provinces of Italy. And what about all this things? The information is made available by institutions for the common benefit. In the future, open-data will be produced in a large amount in favor of the civic life, the transparency and, above all, for your information! You will know, with few clicks, which are schools and universities with the highest results or with the most advanced laboratories. You can find how many people are employed in the public services, with also knowledges about their tasks and their cost for the public administration. Moreover, it is possible to discover where are located services and which city has higher incomes. We want to implement all this information in our database, maintaining the present smart interaction. These information will be useful in many ways: if you're looking for an educational or working guidance, can be helpful retrieve information on where or what is the work demand in a particular field on the local basis. If you need to sell or buy a house you'll know if the price the property is coherent also with services in the territory. Then, you can also personally browse the actual levels of employment and unemployment, even at the local level, not just national.</p>
                    <p lang="en" style="font-size:20px;">Open M.A.T.I.S. - Mapped Acknowledgements on Trentino as Information System.<p>
                </div>
            </div>

            <div id="guide" class="modalDialog">
                <div>
                    <a href="#close" class="close"><img src="./icons/close.png" alt="X"/></a>
                    <h2 lang="en">Guide</h2>
                    <p lang="en">Open M.A.T.I.S. is simple and cross-platform: you can use it on any device, from your smartphone to your personal computer. It works on a municipal basis, so you must enter the name of a municipality (for example, TRENTO) in the search bar, and you'll see on the map all the services available in the chosen area. You'll see also information calculated on the basis of open-data present in the system. Alternatively, you can use the geolocalisation to see what services are in your area, clicking on the top-left arrow in the map.</p>
                </div>
            </div>

            <div id="download" class="modalDialog">
                <div>
                    <a href="#close" class="close"><img src="./icons/close.png" alt="X"/></a>
                    <h2 lang="en">Download</h2>
                    <p lang="en">The project is developed starting from open-data released by the Autonomous Province of Trent and National Institutions. The redistribution of data is regulated in accordance with the license defined by the original source. You can download used CSV files and the geometry of Trentino municipalities. It is added a note on the structure of data tables with references to the original source.</p>
                    <br/>
                    <ul id="index" class="nav nav-sidebar">
                        <li><a href="./data/trentino.kml" lang="en">Municipalities of the province</a></li>
                        <li><a href="./data/note.txt" lang="en">Information note</a></li>
                        <li><a href="./data/ospedali.csv" id="filter-hospital" lang="en">Hospitals</a></li>
                        <li><a href="./data/farmacie.csv" id="filter-drugstore" lang="en">Drugstores</a></li>
                        <li><a href="./data/parafarmacie.csv" id="filter-parapharmacy" lang="en">Parapharmacies</a></li>
                        <li><a href="./data/distributori.csv" id="filter-dispenser" lang="en">Dispensers</a></li>
			<li><a href="./data/grado_apertura_chiusura_sistema_sanitario.csv" id="filter-degree" lang="en">Health system opening and closing degree in Trentino</a></li>
                    </ul>
                </div>
            </div>

            <div id="info" class="modalDialog">
                <div>
                    <a href="#close" class="close"><img src="./icons/close.png" alt="X"/></a>
                    <h2 lang="en">Information</h2>
                    <p><a rel="license" href="http://opendefinition.org/"><img alt="[Open Knowledge]" style="border-width:0" src="http://assets.okfn.org/images/ok_buttons/ok_80x15_blue.png"/></a></p>
                    <p><a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="[Creative Commons - BY 4.0]" style="border-width:0" src="http://i.creativecommons.org/l/by/4.0/80x15.png"/></a></p>
		    <p><a rel="license" href="http://www.gnu.org/licenses/gpl-3.0.html"><img alt="[GNU GPL v.3]" style="border-width:0" src="http://www.gnu.org/graphics/gplv3-88x31.png" /></a></p>
                    <p></p>
                    <p><a href="https://bitbucket.org/opendatatrentino/opendatatrentino" lang="en">Downoad the source-code from BitBucket</a></p>
                    <p><a href="http://challenge.dati.trentino.it/">Trentino Open Data Challenge 2014</a></p>
                    <p lang="en">Only open-source software was used.</p>
		    <p></p>
                    <p>
			<a href="http://it.linkedin.com/pub/lorenzo-waldner/63/853/98b">Lorenzo Waldner</a>, 
			<a href="http://it.linkedin.com/pub/steven-tait/40/58b/1a5/it">Steven Tait</a>, 
			<a href="http://www.linkedin.com/in/jacopodalpozzo">Jacopo Dal Pozzo</a>
		    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div id="mainport" class="row">
        <div class="col-sm-4 col-md-4  sidebar">
            <p class="index-title" lang="en">Index</p>
            <ul id="index" class="nav nav-sidebar">
                <li><a href="#" style="color:#FF0000;" id="filter-hospital" lang="en">Hospitals</a></li>
                <li><a href="#" style="color:#FF9933;" id="filter-drugstore" lang="en">Drugstores</a></li>
                <li><a href="#" style="color:#66CC66;" id="filter-parapharmacy" lang="en">Parapharmacies</a></li>
                <li><a href="#" style="color:#6666FF;" id="filter-dispenser" lang="en">Dispensers</a></li>
            </ul>
            <br/>
            <p class="index-title" lang="en">Number of beds in hospitals for year 2013</p>
            <p id="multi_chois"></p>
            <ul id="index" class="nav nav-sidebar  statistic" lang="en">
                <li><a href="#" lang="en">Ordinary beds:</a></li>
                <li><a href="#" lang="en">Beds for day hospital:</a></li>
                <li><a href="#" lang="en">Beds for day surgery:</a></li>
                <li><a href="#" lang="en">Number of beds:</a></li>
            </ul>
            <br/>
	    <p class="index-title" lang="en">Health system opening and closing degree in Trentino</p>
	    <center>
		<!--<canvas id="piechart" width="250" height="250"></canvas>-->
		<div id="chartContainer" style="height: 300px; width: 100%;"></div>
	    </center>
	    <br/>
        </div>
        <div class="col-sm-8  col-md-8  main">
            <div class="">
                <div id="wrap-map">
                    <div id='map-ui'>
                        <ul id='filters'></ul>
                    </div>
                    <div id='map'></div>
                    <script src="js/map.js" charset="utf-8" type="text/javascript"></script>
                    <script language="JavaScript">
                        var w = window,
                                d = document,
                                e = d.documentElement,
                                g = d.getElementsByTagName('body')[0],
                                x = w.innerWidth || e.clientWidth || g.clientWidth,
                                y = w.innerHeight || e.clientHeight || g.clientHeight;
                        //document.getElementById("wrap-map").style.height = (y-d.getElementById('navbar').style.height)+ "px";
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<script language="JavaScript">
    var onChangeLanguage = function () {
    }
</script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="js/app.js"></script>

</body>
</html>
