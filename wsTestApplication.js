var http = require('http');
var dispatcher = require('httpdispatcher');
var fs = require('fs');
var mysql = require('mysql');
var bind = require('bind');
var path = require('path');
var mine = require('mime');

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: "odct"
});

connection.connect();

dispatcher.onGet('/home', function (req, res, chain) {
    bind.toFile('./tpl/index.tpl', {
        test: 'ciao'
    }, function (data) {
        res.writeHead(200, {
            'Content-Type': 'text/html'
        });
        res.end(data);
    });
});

dispatcher.onGet('/ws/comune', function (req, res, chain) {

    var url_parts = require('url').parse(req.url, true);
    var query = 'SELECT * from comune where Solo_denominazione_in_italiano like "' + url_parts.query.com + '" and codice_provincia = 22';
    console.log(query);

    connection.query(query, function (err, rows) {
        console.log(err);

        if (rows.length < 1) {
            respondZero(res, req, rows)
        } else if (rows.length < 2) {
            respondOne(res, rows[0]);
        } else {
            respondMore(res, rows);
        }
    });
});

function respondOne(res, row) {

    var query = "select 'farmacie' as descrizione,IFNULL(count(*),0) as count from farmacia_  where farmacia_.comune_codice_Istat_del_comune_num = ? union  select 'distributori_farmaci',IFNULL(count(*),0) from distributore_farmaci  where distributore_farmaci.comune_codice_Istat_del_comune_num = ? union  select 'ospedali',IFNULL(count(*),0) from ospedale   where ospedale.comune_codice_Istat_del_comune_num = ? union  select 'posti_letto',IFNULL(sum(Totale_posti_letto),0)  from posti_letto    join ospedale on posti_letto.Codice_struttura  =  ospedale.Codice_struttura     where ospedale.comune_codice_Istat_del_comune_num = ? and Anno = 2013 union  select 'Posti_letto_Day_Hospital',IFNULL(sum(posti_letto.Posti_letto_Day_Hospital),0)  from posti_letto    join ospedale on posti_letto.Codice_struttura  =  ospedale.Codice_struttura     where ospedale.comune_codice_Istat_del_comune_num = ? and Anno = 2013 union  select 'Posti_letto_Ordinari',IFNULL(sum(posti_letto.Posti_letto_Ordinari),0)  from posti_letto    join ospedale on posti_letto.Codice_struttura  =  ospedale.Codice_struttura     where ospedale.comune_codice_Istat_del_comune_num = ? and Anno = 2013 union  select 'Posti_letto_Day_Surgery',IFNULL(sum(posti_letto.Posti_letto_Day_Surgery),0)  from posti_letto    join ospedale on posti_letto.Codice_struttura  =  ospedale.Codice_struttura     where ospedale.comune_codice_Istat_del_comune_num = ? and Anno = 2013 union  select 'inail_competenza',IFNULL(count(*),0) from inail_competenza  where inail_competenza.CodiceIstatComune = ? union   select 'scuola_primaria',IFNULL(count(*),0) from scuola    join comune on comune.CAP = scuola.cap  where des_tipo_scuola like 'SCUOLA PRIMARIA'  and comune.codice_Istat_del_comune_num = ?  union  select 'scuola_secondaria_secondo_grado',IFNULL(count(*),0) from scuola    join comune on comune.CAP = scuola.cap  where des_tipo_scuola like 'SCUOLA SECONDARIA DI SECONDO GRADO'  and comune.codice_Istat_del_comune_num = ?  union  select 'scuola_secondaria_primo_grado',count(*) from scuola    join comune on comune.CAP = scuola.cap   where des_tipo_scuola like 'SCUOLA SECONDARIA DI PRIMO GRADO'  and comune.codice_Istat_del_comune_num = ? ;";
    var query_test = query.split('?').join("" + row.codice_Istat_del_comune_num);
    // console.log(query_test);

    connection.query(query_test, function (err, rows) {
        console.log(err);
        res.writeHead(200, {
            'Content-Type': 'text/javascript'
        });

        for (var i = 0; i < rows.length; i++) {
            row[rows[i].descrizione] = rows[i].count;
        }

        var result = JSON.stringify(row);

        console.log(result);
        res.end(result);
    });
}
function respondMore(res, row) {

    res.writeHead(200, {
        'Content-Type': 'text/javascript'
    });

    var result = JSON.stringify(row);

    console.log(result);
    res.end(result);
}

function respondZero(res, req, row) {


    var url_parts = require('url').parse(req.url, true);
    var query = 'SELECT * from comune where Solo_denominazione_in_italiano like "%' + url_parts.query.com + '%" and codice_provincia = 22';
    console.log(query);

    connection.query(query, function (err, rows) {
        console.log(err);
        //  console.log(rows[0]);
        var output = "";
        if (rows.length < 1) {
            res.writeHead(200, {
                'Content-Type': 'text/javascript'
            });
            res.end("[]");
        } else if (rows.length < 2) {
            respondOne(res, rows[0])
        } else {
            respondMore(res, rows);
        }
        //s res.end(JSON.stringify(rows));
    });
}
http.createServer(
    function (req, res) {

        var url_parts = require('url').parse(req.url, true);
        // console.log(url_parts);

        path.exists('./tpl' + url_parts.path, function (exists) {
            if (exists) {

                fs.readFile('./tpl' + url_parts.path, function (err, data) {
                    if (err) {
                        return console.log(err);
                    }

                    res.writeHead(200, {
                        'Content-Type': mine.lookup('./tpl' + url_parts.path)
                    });

                    res.end(data);
                });

            } else {
                dispatcher.dispatch(req, res);
            }
        })

        // parametri

    }).listen(1337, '0.0.0.0');

console.log('Server running at 0.0.0.0:1337/');
